#ifndef _MEMALLOC_TEST
#define _MEMALLOC_TEST

#include <stdbool.h>

typedef bool (*test)();

void handle_tests();

#endif