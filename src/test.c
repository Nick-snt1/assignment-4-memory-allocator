#include "test.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <stdbool.h>
#include <stdint.h>
#include <sys/mman.h>

#define HEAP_SIZE 32768
#define TEST_SIZE0 (HEAP_SIZE/2)
#define TEST_SIZE1 (HEAP_SIZE/4)
#define TEST_SIZE2 (HEAP_SIZE/4)
#define TEST_SIZE3 (HEAP_SIZE/2)
#define TEST_SIZE4 (HEAP_SIZE/4)
#define OUTPUT stderr

static struct block_header* get_header(void *block) {
    return (struct block_header* )(block - offsetof(struct block_header, contents));
}

static void heap_free(void *heap) {
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
}

// allocate block
bool test0() { 
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(OUTPUT, heap);

    void* block = _malloc(TEST_SIZE0);
    debug_heap(OUTPUT, heap);

    if (!heap || !block) return false;

    _free(block);
    debug_heap(OUTPUT, heap);

    heap_free(heap);
    return true;
}

// allocate several blocks and free one
bool test1() {
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(OUTPUT, heap);

    void* block0 = _malloc(TEST_SIZE1), *block1 = _malloc(TEST_SIZE1);
    debug_heap(OUTPUT, heap);

    if (!heap || !block0 || !block1) return false;
    if (get_header(block0)->capacity.bytes != TEST_SIZE1 || 
        get_header(block1)->capacity.bytes != TEST_SIZE1) return false;

    _free(block0);
    debug_heap(OUTPUT, heap);

    if (!get_header(block0)->is_free) return false;

    _free(block1);
    heap_free(heap);

    return true;
}

// allocate several blocks and free two
bool test2() {
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(OUTPUT, heap);

    void *block0 = _malloc(TEST_SIZE2),
         *block1 = _malloc(TEST_SIZE2),
         *block2 = _malloc(TEST_SIZE2);
    debug_heap(OUTPUT, heap);

    if (!heap || !block0 || !block1 || !block2) return false;
    if (get_header(block0)->capacity.bytes != TEST_SIZE2 ||
        get_header(block1)->capacity.bytes != TEST_SIZE2 ||
        get_header(block2)->capacity.bytes != TEST_SIZE2 ) return false;

    _free(block0), _free(block1);
    debug_heap(OUTPUT, heap);

    if (!get_header(block0)->is_free || !get_header(block1)->is_free) return false;

    _free(block2);
    heap_free(heap);

    return true;
}

//  expand heap if not enough memory
bool test3() {
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(OUTPUT, heap);

    void *block0 = _malloc(TEST_SIZE3),
         *block1 = _malloc(TEST_SIZE3),
         *block2 = _malloc(TEST_SIZE3);
    debug_heap(OUTPUT, heap);

    if (!heap || !block0 || !block1 || !block2) return false;
    if (get_header(block0)->capacity.bytes != TEST_SIZE3 ||
        get_header(block1)->capacity.bytes != TEST_SIZE3 ||
        get_header(block2)->capacity.bytes != TEST_SIZE3) return false;

    _free(block0), _free(block1), _free(block2);
    heap_free(heap);

    return true;
}

// expand heap if not enough memory but regions not one after another
bool test4() {
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) return false;
    debug_heap(stdout, heap);

    void *block0 = _malloc(HEAP_SIZE);
    debug_heap(stdout, heap);

    void *mapped_region = mmap(heap+REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | 0x20, -1, 0);

    if (mapped_region == MAP_FAILED) return false;

    void *block1 = _malloc(TEST_SIZE4);
    debug_heap(stdout, heap);

    if (!block0 || !block1) return false;
    if (get_header(block0)->capacity.bytes != HEAP_SIZE ||
        get_header(block1)->capacity.bytes != TEST_SIZE4) return false;

    _free(block0), _free(block1);
    munmap(mapped_region, REGION_MIN_SIZE);
    heap_free(heap);

    return true;
}

void handle_tests() {
    size_t test_ammount = 5;
    test tests[] = {test0, test1, test2, test3, test4};
    size_t count = 0;
    for (size_t i = 0; i < test_ammount; i++) {
        fprintf(OUTPUT,"\n--- Test #%" PRId64 " started ---\n\n", i);
        if (tests[i]()) {
            fprintf(OUTPUT, "\n--- Test #%" PRId64 " passed ---\n", i);
            count++;
        } else fprintf(OUTPUT, "\n--- Test #%" PRId64 " failed ---\n", i);
    }
    fprintf(OUTPUT, "\n--- %" PRId64 "/%" PRId64  " tests passed ---\n", count, test_ammount);
}